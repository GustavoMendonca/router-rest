# Controlador de link TCP/IP

Container docker que age como intermediário de conexão com dispositivo IP. Controla estado do link (UP and DOWN).

# API Endpoints

## /linkup
Para criar uma conexão que encaminhe as mensagens enviadas para <porta_origem> para <ip_destino>:<porta_destino>, enviar post em <ip_api>:<porta_api>/linkup com content-type 'application/json' e corpo:
```json
{
    "ip":"192.168.5.200",
    "porta_origem":102,
    "porta_detino":4678
}

```

## /linkdown
Para excluir uma conexão, enviar post em <ip_api>:<porta_api>/linkdown com content-type 'application/json' e corpo:
```json
{
    "ip": "192.168.5.200",
    "porta": 102,
    "force":true 
}

```
**obs**: A remoção da regra da iptables que redireciona os pacotes de rede não basta para derrubar conexões persistentes já online. Para remover estas conexões, enviar parâmentro **"force":true** (opcional).


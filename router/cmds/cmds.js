(function() {
  'use strict'

  const { exec, execSync } = require('child_process')

  let routes = []

  exports.uplink = (ip, srcPort, dstPort, cb) => {
    let route = routes.find((r) => r.srcPort === srcPort)
    if (!route)
      exec(['link-up.sh', ip, srcPort, dstPort].join(' '), (err, stdout, stderr) => {
        if (err) cb(`exec error: ${err}`)
        else {
          routes.push({ip:ip, srcPort:srcPort, dstPort:dstPort})
          if(stdout) console.log(`stdout: ${stdout}`)
          if(stderr) console.log(`stderr: ${stderr}`)
          cb()
        }
      })
    else cb(`um roteamento na porta ${srcPort} já existe`)
  }

  exports.downlink = (ip, srcPort, dstPort, force=false, cb) => {
    let route = routes.find((r) => r.srcPort === srcPort)
    if (!route) 
      cb(`roteamento na porta ${srcPort} não encontrado`)
    else 
      if (!force){
        exec(['link-down.sh', ip, srcPort, dstPort].join(' '), (err, stdout, stderr) => {
          if (err) cb(`exec error: ${err}`)
          else {
            let index = routes.findIndex((e) => {
              return e.ip === ip && e.srcPort === srcPort && e.dstPort === dstPort
            })
            if (index > -1) routes.splice(index, 1)
            if(stdout) console.log(`stdout: ${stdout}`)
            if(stderr) console.log(`stderr: ${stderr}`)
            cb()
          }
        })
      } 
      else {
        exec('flush-all.sh', (err, stdout, stderr) => {
          if (err) cb(`exec error: ${err}`)
          else {
            let index = routes.findIndex((e) => {
              return e.ip === ip && e.srcPort === srcPort && e.dstPort === dstPort
            })
            if (index > -1) routes.splice(index, 1)
            routes.forEach((r) => {
              console.log('reinserindo', r, 'cmd ', ['link-up.sh', r.ip, r.srcPort, r.dstPort].join(' '))
              execSync(['link-up.sh', r.ip, r.srcPort, r.dstPort].join(' '), (err, stdout, stderr) => {
                if (err) cb(`exec error: ${err}`)
                else {
                  if(stdout) console.log(`stdout: ${stdout}`)
                  if(stderr) console.log(`stderr: ${stderr}`)
                }
              })
            })
            cb()
          }
        })
      }
  }

  exports.flush = (cb) => { console.assert('flush')
    exec('flush-all.sh', (err, stdout, stderr) => {
      if (err) cb(`exec error: ${err}`)
      else {
        routes = []
        if(stdout) console.log(`stdout: ${stdout}`)
        if(stderr) console.log(`stderr: ${stderr}`)
        cb()
      }
    })
  }
})()
#!/bin/sh

echo "Excluindo todos os roteamentos..."

iptables -F
iptables -t nat -F

iptables -L FORWARD
iptables -t nat -L PREROUTING

echo "roteamento cancelado!"
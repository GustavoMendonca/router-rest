#!/bin/sh

echo "Cancelando roteamento da porta $2 para ip $1:$3..."

iptables -t nat -D PREROUTING -p tcp --dport $2 -j DNAT --to-destination $1:$3
iptables -D FORWARD -p tcp -d $1 --dport $3 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -D POSTROUTING -j MASQUERADE

iptables -L FORWARD
iptables -t nat -L PREROUTING

echo "roteamento cancelado!"
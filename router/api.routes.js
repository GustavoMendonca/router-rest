(function() {
  const express = require('express')
  const router = express.Router()
  const cmds = require('./cmds/cmds')
 
  router
    .get('/', (req, res) => { res.status(200).json('router OK') })
    .post('/uplink', (req, res) => {
      let ip = req.body.ip
      let srcPort = parseInt(req.body.porta_origem, 10)
      let dstPort = parseInt(req.body.porta_destino, 10)
      if (srcPort > 0 && dstPort > 0 && ip) {
        cmds.uplink(ip, srcPort, dstPort, (err) => {
          res.status((err) ? 500 : 200).json(err)
        }) 
      } else {
        res.status(400).json('Http POST com parâmetros inválidos')
      }
    })
    .post('/downlink', (req, res) => {
      let ip = req.body.ip
      let force = req.body.force
      let srcPort = parseInt(req.body.porta_origem, 10)
      let dstPort = parseInt(req.body.porta_destino, 10)
      if (srcPort > 0 && dstPort > 0 && ip) {
        cmds.downlink(ip, srcPort, dstPort, force, (err) => {
          res.status((err) ? 500 : 200).json(err)
        })
      } else {
        res.status(400).json('Http POST com parâmetros inválidos')
      }
    })
    .post('/flush', (req, res) => {
      cmds.flush((err) => {
        res.status((err) ? 500 : 200).json(err)
      })
    })
    
  module.exports = router

})()
(function () {
  const express = require('express')
  const app = express()
  const apiRoutes = require('./api.routes')
  const bodyParser = require('body-parser')
  
  const apiPort = process.env.API_PORT || 7777

  app.use(bodyParser.json())

  app.use((req, res, next) => {
    let now = new Date
    console.log(`[${now.toISOString().replace('T', ' ').slice(0, 23)}] ${req.method} em ${req.path}`)
    next()
  })

  app.use('/', apiRoutes)

  // inicia API de controle  
  app.listen(apiPort, function () {
    console.log('API do router iniciado na porta', apiPort)
  })

})()